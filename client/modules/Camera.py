# -*- coding: utf-8-*-
import re
from picamera import PiCamera
import datetime
import socket
import time
import telebot
import shlex
import subprocess
import os


WORDS = ["CAMERA"]

def genFileName():
    return datetime.datetime.now().strftime("%Y%m%d_%H%M%S")

def handle(text, mic, profile):
    bot = None
    bot_key = profile["keys"]["TELEGRAM_BOT"]
    userId = profile["keys"]["TELEGRAM_USERID"]
    del_data = profile["delete_after_sending"]
    if bot_key and userId:
        bot = telebot.TeleBot(bot_key)

    def chooseCameraOption(text):
        with PiCamera() as camera:
            camera.resolution = (
                profile["camera"]["resolution"]["w"],
                profile["camera"]["resolution"]["h"],
            )
            camera.start_preview()

            if text is None or isValid(text, "Nothing"):
                mic.say("Okay, you know where you can find me, right?")

                return False

            elif isValid(text, "Take a photo"):
                mic.say("Yeah, sure, three, two, one, and")
                photoName = "%s/%s.jpg" % (profile["paths"]["photo"], genFileName())
                camera.capture(photoName)
                
                if bot:
                    with open(photoName) as photo:
                        bot.send_photo(userId, photo)
                    if del_data:
                        os.remove(photoName)

                    mic.say("Nice! Here we go! What else?")

                return True

            elif isValid(text, "Take a video"):
                mic.say("The recording is started")
                videoName = "%s/%s" % (profile["paths"]["video"], genFileName())
                camera.start_recording(videoName + ".h264")

                try:
                    while "STOP" not in [i for i in mic.passiveListen("STOP", profile["limits"]["video"], False)][1]:
                        camera.wait_recording(0.5)
                    camera.stop_recording()

                    subprocess.check_output(
                        shlex.split("MP4Box -add {f}.h264 {f}.mp4".format(f=videoName)),
                        stderr=subprocess.STDOUT
                    )
                    os.remove(videoName + ".h264")

                    if bot:
                        with open(videoName + ".mp4") as video:
                            bot.send_video(userId, video)
                        if del_data:
                            os.remove(videoName + ".mp4")

                    mic.say("The video is recorded, what else?")

                    return True

                except Exception as e:
                    mic.say("Something is wrong with camera. See logs")
                    print "{}".format(e)

                    return False
            
            elif isValid(text, "Stream"):
                server_socket = socket.socket()
                server_socket.bind((profile["stream"]["host"], profile["stream"]["port"]))
                server_socket.listen(0)
                connection = server_socket.accept()[0].makefile("wb")

                try:
                    camera.start_recording(connection, format="h264")
                    mic.say("The stream is started")

                    while "STOP" not in [i for i in mic.passiveListen("STOP", profile["limits"]["stream"], False)][1]:
                        camera.wait_recording(0.5)
                    camera.stop_recording()

                    mic.say("The stream is stoped, what else?")

                except Exception as e:
                    mic.say("Something is wrong with camera. See logs")
                    print "{}".format(e)

                    return False

                finally:
                    connection.close()
                    server_socket.close()
                
                return True
            
            elif isValid(text, "Telegram"):
                mic.say("The controll is in the telegram now. Please send the command")
                videoName = "%s/%s" % (profile["paths"]["video"], genFileName())
                handle_connection = True

                try:
                    @bot.message_handler(commands=["camera"])
                    def camera_start(message):
                        try:
                            markup = telebot.types.ReplyKeyboardMarkup(row_width=2)
                            markup.add(
                                telebot.types.KeyboardButton("Take photo"),
                                telebot.types.KeyboardButton("Start video"),
                                telebot.types.KeyboardButton("Stop video"),
                                telebot.types.KeyboardButton("Exit"),
                            )
                            bot.send_message(userId, "Choose a command:", reply_markup=markup)
                        except Exception as e:
                            print "THIS IS:{}".format(e)

                    @bot.message_handler(regexp="Take photo")
                    def take_photo(message):
                        photoName = "%s/%s.jpg" % (profile["paths"]["photo"], genFileName())
                        mic.say("Yeah, sure, three, two, one, and")
                        camera.capture(photoName)
                        
                        if bot:
                            with open(photoName) as photo:
                                bot.send_photo(userId, photo)
                            if del_data:
                                os.remove(photoName)
                        mic.say("Nice! Here we go! What else?")

                    @bot.message_handler(regexp="Start video")
                    def start_video(message):
                        mic.say("The recording is started")
                        camera.start_recording(videoName + ".h264")

                    @bot.message_handler(regexp="Stop video")
                    def stop_video(message):
                        mic.say("The recording is stoped")
                        camera.stop_recording()
                        subprocess.check_output(
                            shlex.split("MP4Box -add {f}.h264 {f}.mp4".format(f=videoName)),
                            stderr=subprocess.STDOUT
                        )
                        os.remove(videoName + ".h264")

                        with open(videoName + ".mp4") as video:
                            bot.send_video(userId, video)
                        if del_data:
                            os.remove(videoName + ".mp4")

                    @bot.message_handler(regexp="Exit")
                    def close_connection(message):
                        handle_connection = False
                        if not handle_connection:
                            raise Exception("Connection with telegram handlers closed")

                    if handle_connection:
                        bot.polling()
                except Exception:
                    mic.say("The controll returned to the microphone. So, what do you want to do with camera?")
                    return True
            
            else:
                mic.say("I have no such functionality, sorry")
                
                return True

    mic.say("What do you want me to do with camera?")

    while True:
        if chooseCameraOption(mic.activeListen()):
            continue
        else:
            break

def isValid(text, pattern=WORDS[0]):
    return bool(re.search("%s" % pattern, text, re.IGNORECASE))
